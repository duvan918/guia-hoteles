// Script para tooltip
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl)
})

// Script para popover
var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
    return new bootstrap.Popover(popoverTriggerEl)
})

// JQuery
$(function(){
    $('#contacto').on('show.bs.modal', function(e){
        console.log('El modal se está mostrando');
        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-primary');
        $('#contactoBtn').prop('disabled', true);
    });
    
    $('#contacto').on('shown.bs.modal', function (e){
        console.log('El modal se mostró');
    });
    $('#contacto').on('hide.bs.modal', function (e){
        console.log('El modal se oculta');
    });
    
    $('#contacto').on('hidden.bs.modal', function (e){
        console.log('El modal se ocultó');
        $('#contactoBtn').prop('disabled', false);
    });
})

// Scripts formas alternativas
// $(function(){
//     $("[data-bs-toggle='tooltip']").tooltip();
//     $("[data-bs-toggle='popover']").popover();
    
//     $('.carousel').carousel({
//         interval: 2000
//     });
    
//     $('#contacto').on('show.bs.modal', function (e){
//         console.log('El modal se está mostrando');
//     });
// })


